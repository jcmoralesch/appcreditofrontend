import { Component, OnInit,ViewChild } from '@angular/core';
import {ClienteService} from '../../model/service/cliente.service';
import {Cliente} from '../../model/class/cliente';
import {MatTableDataSource,MatSort,MatPaginator} from '@angular/material';
import {UsuarioService} from '../../model/service/usuario.service';
import {LoginService} from '../../model/service/login.service';
import {Credito} from '../../model/class/credito';
import {CreditoService} from '../../model/service/credito.service';
import {TipoPago} from '../../model/class/tipo-pago';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  title:string='Cliente';
  clientes:Cliente[];
  paginador:any;
  dataSource: any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ubicacion:string='';
  cliente:Cliente = new Cliente();
  credito:Credito= new Credito();
  tipoPagos:TipoPago[];
  isDisabled:true;
  public loading:boolean=false;
  public mostrarNavLateral:boolean=true;
  public texto: string =  'Ocultar';


  constructor(public clienteService:ClienteService,
              public loginService:LoginService,
              private usuarioService:UsuarioService,
              public creditoService:CreditoService) { }

  ngOnInit() {
    this.findUsuarioById();
    this.findAllTipoPago();
  }

  displayedColumns = ['Codigo',"Nombre",'Apellido','Telefono','Ruta','Ciclo','Fecha','Status','Acciones'];

  findUsuarioById():void{
    this.usuarioService.findById(this.loginService.id).subscribe(
      ubicacion=>{
        this.findAllByUbicacionAndStatusA(ubicacion.ubicacion);
        this.ubicacion=ubicacion.ubicacion;
      }
    )
  }

  findAllClienteByUbicacion(ubicacion:string):void{
    this.clienteService.findAllByUbicacion(ubicacion).subscribe(
      response=>{
        this.dataSource=new MatTableDataSource();
        this.dataSource.data=response;
         this.dataSource.sort=this.sort;
         this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
    )
  }

  tableFilter(): (data: Cliente, filter: string) => boolean {
      let filterFunction = function(data, filter): boolean {
       return data.nombre.indexOf(filter) != -1
       || data.apellido.indexOf(filter) != -1
       || data.codigo.ruta.codigo.indexOf(filter) != -1;

      }
      return filterFunction;
   }

   applyFilter(filterValue: any) {
     filterValue = filterValue.trim();
     filterValue = filterValue.toUpperCase();
     this.dataSource.filter=filterValue;
   }


  asignarCredito(row){
    this.cliente=row;
    this.clienteService.abrirModal();
  }

  cerrarModal():void{
    this.clienteService.cerrarModal();
  }

  findAllTipoPago():void{
    this.clienteService.findAllTipoPago().subscribe(
      tipoPago=>this.tipoPagos=tipoPago
    )
  }

  registrarCredito(){
        this.loading=true;
        this.credito.monto=this.cliente.cantidad;
        this.credito.cliente=this.cliente;
        this.credito.tipoPago=this.cliente.tipoPago;
        this.actualizarDatosCliente();

        this.creditoService.store(this.credito).subscribe(
           credito=>{
             this.loading=false;
             this.cerrarModal();
             Swal.fire('Credito asignado a',`${credito.cliente.nombre}`,'success');
             this.findUsuarioById();
           },
           err=>{
             this.loading=false;
           }

        )
  }

  public delete(row:Cliente):void{

    Swal.fire({
        title: 'Esta seguro?',
        text: `De eliminar al cliente  ${row.nombre} !`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, Eliminar !'
      }).then((result) => {

        if (result.value) {
         this.clienteService.delete(row.id).subscribe(
           response=>{
             this.dataSource.data=this.dataSource.data.filter(clie=> clie !==row);
             Swal.fire(
               'Eliminado!',
               `El usuario ${row.nombre} ha sido eliminado`,
               'success'
             )
           }
         )

        }
      })

  }

  public findAllByUbicacionAndStatus(ubicacion:string):void{
    this.clienteService.findAllByUbicacionAndStatus(ubicacion).subscribe(
      response=>{
        this.dataSource=new MatTableDataSource();
        this.dataSource.data=response;
         this.dataSource.sort=this.sort;
         this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
    )
  }

  public findAllByUbicacionAndStatusA(ubicacion:string):void{
    this.clienteService.findAllByUbicacionAndStatusA(ubicacion).subscribe(
      response=>{
        this.dataSource=new MatTableDataSource();
        this.dataSource.data=response;
         this.dataSource.sort=this.sort;
         this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
    )
  }

  public cambiaVisibilidad() {
    this.texto = (this.mostrarNavLateral) ?  'Mostrar' : 'Ocultar';
    this.mostrarNavLateral = !this.mostrarNavLateral; 
  }

  actualizarDatosCliente():void{
    this.clienteService.updateDatosCliente(this.cliente).subscribe();
  }
}
