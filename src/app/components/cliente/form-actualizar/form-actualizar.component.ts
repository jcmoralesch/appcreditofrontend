import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../model/service/login.service';
import {UsuarioService} from '../../../model/service/usuario.service';
import {Router,ActivatedRoute} from '@angular/router';
import {Cliente }from '../../../model/class/cliente';
import {ClienteService } from '../../../model/service/cliente.service';
import Swal from 'sweetalert2';
import { Codigo } from '../../../model/class/codigo';
import { CodigoService } from '../../../model/service/codigo.service';

@Component({
  selector: 'app-form-actualizar',
  templateUrl: './form-actualizar.component.html',
  styleUrls: ['./form-actualizar.component.css']
})
export class FormActualizarComponent implements OnInit {

    public cliente:Cliente= new Cliente();
    public errores:string[];
    title:string='Actualizar datos cliente'
    public codigos:Codigo[];
    public loading:boolean=false;
    public idCodigoActual:Codigo;
    public verificarCambio:boolean=false;

  constructor(private clienteService:ClienteService,
              private router:Router,
              private loginService:LoginService,
              private usuarioService:UsuarioService,
              private activatedRoute:ActivatedRoute,
              private codigoService:CodigoService) { }

  ngOnInit() {
    this.findAllRutaByUbicacion();
    this.cargarDatosClienteFormulario();
  }


  public findAllRutaByUbicacion():void{
     this.usuarioService.findById(this.loginService.id).subscribe(
       ubicacion=>this.cargarCodigo(ubicacion.ubicacion));
   }

   public cargarDatosClienteFormulario():void{
     this.activatedRoute.params.subscribe(
       params=>{
         let id=params['id']
         if(id){
           this.clienteService.getCliente(id).subscribe(
             (cliente)=>{
               this.cliente=cliente
               this.idCodigoActual=cliente.codigo;
             }
           )};
       });
   }

   public updateCliente():void{
     this.loading=true;
     this.clienteService.updateCliente(this.cliente)
     .subscribe(cliente=>{
       this.loading=false;
       this.router.navigate(['/cliente']);
       Swal.fire('Datos actualizado',`${cliente.nombre} actualizados con exito`,'success');
       if(this.verificarCambio){
         this.codigoService.updateStatusCodigo(this.idCodigoActual).subscribe(
           response=>console.log('Codigo actualizado')
         )
       }
     },
     err=>this.loading=false
     )
   }

   public cargarCodigo(ubicacion:string):void{
    this.codigoService.findAllCodigoByRutaUbicacion(ubicacion).subscribe(
      response=>{
        this.codigos=response;
        this.codigos.push(this.cliente.codigo);
      }
    )
  }

  public obtenerCodigo():void{
     this.verificarCambio=true;
  }

   public compararCodigo(o1:Codigo,o2:Codigo):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
   }
}
