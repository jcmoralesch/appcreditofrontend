import { Component, OnInit } from '@angular/core';
import {Cliente }from '../../model/class/cliente';
import {ClienteService } from '../../model/service/cliente.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {TipoPago} from '../../model/class/tipo-pago';
import {CreditoService} from '../../model/service/credito.service';
import {Credito} from '../../model/class/credito';
import {LoginService} from '../../model/service/login.service';
import {UsuarioService} from '../../model/service/usuario.service';
import {RutaService} from '../../model/service/ruta.service';
import { Codigo } from '../../model/class/codigo';
import { CodigoService } from '../../model/service/codigo.service';
import { Ruta } from 'src/app/model/class/ruta';

@Component({
  selector: 'app-form-cliente',
  templateUrl: './form-cliente.component.html'
})
export class FormClienteComponent implements OnInit {

  title:string="Formulario de registro de clientes";
  public cliente:Cliente= new Cliente();
  public tipoPagos:TipoPago[];
  public errores:string[];
  public rutas:Ruta[];
  private credito:Credito = new Credito();
  public codigos:Codigo[];
  public loading:boolean=false;

  constructor(private clienteService:ClienteService,
              private router:Router,
              private creditoService:CreditoService,
              private loginService:LoginService,
              private usuarioService:UsuarioService,
              private rutaService:RutaService,
              private codigoService:CodigoService) {
  }

  ngOnInit() {
     this.findAllTipoPago();
     this.findAllRutaByUbicacion();
     this.cargarDesdeModal();
  }

  store():void{
    this.loading=true;
    this.clienteService.store(this.cliente).subscribe(
      cliente=>{
        this.storeCredito(cliente);
        this.loading=false;
        this.router.navigate(['/cliente']);
        Swal.fire('Nuevo cliente',`El cliente ${cliente.nombre} ha sido creado con exito `,'success');
      },
      err=>{
        this.errores=err.error.errors as string[];
        this.loading=false;
      }
    );

  }

  storeCredito(cliente:Cliente):void{
      this.credito.tipoPago=this.cliente.tipoPago;
      this.credito.monto=this.cliente.cantidad;
      this.credito.cliente=cliente;
      this.creditoService.store(this.credito).subscribe()
  }

  findAllTipoPago():void{
    this.clienteService.findAllTipoPago().subscribe(
      tipoPago=>this.tipoPagos=tipoPago
    )
  }

  findAllRutaByUbicacion():void{
    this.usuarioService.findById(this.loginService.id).subscribe(
      ubicacion=>{
        this.rutaService.findRutaByUbicacion(ubicacion.ubicacion).subscribe(
          rutas=>this.rutas=rutas
        );
        this.cargarCodigo(ubicacion.ubicacion);
      });
  }

  private cargarCodigo(ubicacion:string):void{
    this.codigoService.findAllCodigoByRutaUbicacion(ubicacion).subscribe(
      response=>this.codigos=response
    )
  }

  public abrirModalCodigo():void{

      this.codigoService.abrirModal();

  }
  private cargarDesdeModal():void{
    this.codigoService.notificarCambio.subscribe(

      response=>this.findAllRutaByUbicacion()
      
    )
  }

  public findAllByCodigo(codigo:string){
    this.codigoService.findAllCodigoLibre(codigo).subscribe(
      response=>this.codigos=response
    )
  }

}
