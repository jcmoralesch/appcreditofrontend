import { Component, OnInit,Input } from '@angular/core';
import {FechaPago} from '../../../model/class/fecha-pago';
import {EstadoCuentaFechaService} from '../../../model/service/estado-cuenta-fecha.service';

@Component({
  selector: 'app-estado-cuenta-fecha',
  templateUrl: './estado-cuenta-fecha.component.html',
  styleUrls: ['./estado-cuenta-fecha.component.css']
})


export class EstadoCuentaFechaComponent implements OnInit {

  @Input() fechaPago:FechaPago;
  constructor(public estadoFechaService:EstadoCuentaFechaService) { }

  ngOnInit() {
  }

  cerrarModal(){
    this.estadoFechaService.cerrarModal();
  }

}
