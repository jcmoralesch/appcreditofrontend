import { Component, OnInit, ViewChild } from "@angular/core";
import { FechaPagoService } from "../../model/service/fecha-pago.service";
import { MatTableDataSource, MatSort, MatPaginator } from "@angular/material";
import { FechaPago } from "../../model/class/fecha-pago";
import { RutaService } from "../../model/service/ruta.service";
import { LoginService } from "../../model/service/login.service";
import { UsuarioService } from "../../model/service/usuario.service";
import * as jsPDF from "jspdf";
import "jspdf-autotable";
import { EstadoCuentaFechaService } from "../../model/service/estado-cuenta-fecha.service";
import { Deposito } from "../../model/class/deposito";
import Swal from "sweetalert2";
import { DepositoService } from "../../model/service/deposito.service";
import { Personal } from '../../model/class/personal';
import { PersonalService } from '../../model/service/personal.service';
import { EstadoCuentaService } from '../../model/service/estado-cuenta.service';
import { EstadoCuenta } from '../../model/class/estado-cuenta';
import { Ruta } from 'src/app/model/class/ruta';

@Component({
  selector: "app-fecha-pago",
  templateUrl: "./fecha-pago.component.html",
  styleUrls: ["./fecha-pago.component.css"]
})
export class FechaPagoComponent implements OnInit {
  dataSource: any;
  fechaPagos: FechaPago[];
  fechaPagosVencida:FechaPago[];
  estadoCuentaCreditoVencido:EstadoCuenta[];
  estadoCuentaActivo:EstadoCuenta[];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  rutas: Ruta[];
  ubicacion: string;
  fechaPagoSeleccionado: FechaPago;
  agregarFechas: Array<EstadoCuenta> = [];
  public agregarClientesADeposito: Array<EstadoCuenta> = [];
  public loading: boolean = false;
  public verificarCargaDatos: boolean = false;
  public refInput:any;
  public personal:Personal;
  public numeral:string;
  public indexnum:number=1;
  public sumarCuotas:number;
  public sumarMora:number;
  public sumCuotaMora:number;
  public sumTotal:number;
  public formaPago:string;

  constructor(
    public fechaPagoService: FechaPagoService,
    private rutaService: RutaService,
    public loginService: LoginService,
    private usuarioService: UsuarioService,
    private estadoFechaService: EstadoCuentaFechaService,
    private depositoService: DepositoService,
    private personalService:PersonalService,
    private estadoCuentaService:EstadoCuentaService
  ) {}

  ngOnInit() {
    this.findRutaByUbicacion();
  }

  findRutaByUbicacion(): void {
    this.usuarioService.findById(this.loginService.id).subscribe(ubicacion => {
      this.rutaService
        .findRutaByUbicacion(ubicacion.ubicacion)
        .subscribe(rutas => (this.rutas = rutas));
      this.ubicacion = ubicacion.ubicacion;
    });
  }

  displayedColumns = [
    "Ruta",
    "Nombre",
    "Apellido",
    "Cuota",
    "Saldo",
    "PagosFaltantes",
    "Acciones"
  ];


  public findAllByFechaAndRutaReporte(ruta: string) {
    let cargarFechas: Array<EstadoCuenta> = [];
    this.agregarFechas = [];
    this.fechaPagos=[];
    this.estadoCuentaActivo=[];
    this.fechaPagosVencida=[];
    this.estadoCuentaCreditoVencido=[];
    this.sumarCuotas=0;
    this.sumarMora=0;
    this.sumCuotaMora=0;
    this.sumTotal=0;
    this.formaPago='';

    this.buscarPersonal(ruta);
    this.estadoCuentaService.findAllByUbicacionForReporte(ruta).subscribe(
      response => {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();

        response.forEach((item:EstadoCuenta)=>{
          const   fecha:Date = new Date();
          const fechaActual=new Date(fecha.getFullYear(),fecha.getMonth(),fecha.getDate());
          const fechaVenci= new Date(item.credito.fechaVencimiento);

          if(fechaVenci<fechaActual){
             this.estadoCuentaCreditoVencido.push(item);
          }
          else{
           this.formaPago=item.credito.tipoPago.formaPago;
           this.sumarCuotas +=item.credito.cuota;
           this.sumarMora+= item.credito.mora.moraGenerada;
           this.sumCuotaMora+=item.credito.cuota *(item.credito.mora.diasMora + 1);
           this.sumTotal+= item.credito.cuota * (item.credito.mora.diasMora + 1) +item.credito.mora.moraGenerada;
           this.estadoCuentaActivo.push(item);
          }
        }); 
        cargarFechas = response;
        this.verificarCargaDatos = true;
        cargarFechas.forEach((item: EstadoCuenta) => {
          this.agregarFechas.push(item);
        });  
      },
      err => {
        console.error("Observer tiene un error" + err);
      }
    );
  }

/*
  findAllByFechaAndRuta(ruta: string) {
    let cargarFechas: Array<FechaPago> = [];
    this.agregarFechas = [];
    this.fechaPagos=[];
    this.fechaPagosVencida=[];
    this.sumarCuotas=0;
    this.sumarMora=0;
    this.sumCuotaMora=0;
    this.sumTotal=0;
    this.buscarPersonal(ruta);
    this.fechaPagoService.findAllFechaPagoAndRuta(ruta).subscribe(
      response => {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();

        response.forEach((item:FechaPago)=>{
          const   fecha:Date = new Date();
          const fechaActual=new Date(fecha.getFullYear(),fecha.getMonth(),fecha.getDate());
          const fechaVenci= new Date(item.credito.fechaVencimiento);

          if(fechaVenci<fechaActual){
             this.fechaPagosVencida.push(item);
          }
          else{
           this.sumarCuotas +=item.credito.cuota;
           this.sumarMora+= item.credito.mora.moraGenerada;
           this.sumCuotaMora+=item.credito.cuota *(item.credito.mora.diasMora + 1);
           this.sumTotal+= item.credito.cuota * (item.credito.mora.diasMora + 1) +item.credito.mora.moraGenerada;
           this.fechaPagos.push(item);
          }
        }); 
        cargarFechas = response;
        this.verificarCargaDatos = true;
        cargarFechas.forEach((item: FechaPago) => {
          this.agregarFechas.push(item);
        });
      },
      err => {
        console.error("Observer tiene un error" + err);
      }
    );
  }

  */

  private buscarPersonal(ruta:string):void{
    this.personalService.getPersonalByRuta(ruta).subscribe(
      response=>{
        this.numeral=response.ruta.codigo.slice(1,2);
        this.personal=response;
      })
  }

  tableFilter(): (data: FechaPago, filter: string) => boolean {
    let filterFunction = function(data, filter): boolean {
      return (
        data.credito.cliente.nombre.indexOf(filter) != -1 ||
        data.credito.cliente.apellido.indexOf(filter) != -1
      );
    };
    return filterFunction;
  }

  applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toUpperCase();
    this.dataSource.filter = filterValue;
  }

  limpiar(event:any): void {
    this.refInput=event;
  }

  infoCompleta(row) {
    this.fechaPagoSeleccionado = row;
    this.estadoFechaService.abrirModal();
  }

  registrarPago(row: EstadoCuenta) {
    
    if(this.refInput!=undefined){
      this.refInput.target.value='';
      this.refInput.target.focus();
    }
  
    let v: number = this.agregarFechas.indexOf(row);
    this.agregarFechas.splice(v, 1);
    this.dataSource = new MatTableDataSource();
    this.dataSource.data = this.agregarFechas;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.dataSource.filterPredicate = this.tableFilter();

    this.agregarClientesADeposito.unshift(row);
  }

  public eliminarItemCliente(id: number): void {
    this.agregarClientesADeposito = this.agregarClientesADeposito.filter(
      (item: EstadoCuenta) => id !== item.id
    );
  }

  public actualizarCuota(id: number, event: any, saldo: number): void {
    let cuota = event.target.value;
    if (saldo < cuota) {
      Swal.fire(
        'Advertencia',
        'Esta Ingresando una cantidad mayor al SALDO',
        'warning'
      );
      event.target.value = '';
      return;
    }
    this.agregarClientesADeposito = this.agregarClientesADeposito.map(
      (item: EstadoCuenta) => {
        if (id === item.credito.id) {
          item.credito.cuota = parseFloat(cuota);
          item.credito.cuota = item.credito.cuota;
        }
        return item;
      }
    );
  }

  public actualizarMora(id: number, event: any, moraActual: number): void {
    let mora = event.target.value;
    if (moraActual < mora) {
      Swal.fire(
        "Advertencia",
        "Esta Ingresando una cantidad mayor a la MORA generada",
        "warning"
      );
      event.target.value = "0";
      return;
    }
    this.agregarClientesADeposito = this.agregarClientesADeposito.map(
      (item: EstadoCuenta) => {
        if (id === item.credito.id) {
          item.credito.mora.moraGenerada = parseFloat(mora);
          item.credito.mora.moraGenerada = item.credito.mora.moraGenerada;
        }
        return item;
      }
    );
  }

  public calcularGranTotal(): number {
    let total: number = 0;
    this.agregarClientesADeposito.forEach((item: EstadoCuenta) => {
      total += item.credito.cuota;
    });
    return total;
  }

  public calcularGranTotalMora(): number {
    let total: number = 0;
    this.agregarClientesADeposito.forEach((item: EstadoCuenta) => {
      total += item.credito.mora.moraGenerada;
    });
    return total;
  }

  public storeDeposito(): void {
    const arrayDeposito: Array<Deposito> = [];
    this.agregarClientesADeposito.forEach((item: EstadoCuenta) => {
      let deposito: Deposito = new Deposito();

      deposito.credito = item.credito;
      deposito.cantidad = item.credito.cuota;
      deposito.mora=item.credito.mora.moraGenerada;
      arrayDeposito.push(deposito);
    });
    this.loading = true;
    this.depositoService.storeArrayDeposito(arrayDeposito).subscribe(
      response => {
        Swal.fire("Exito", "Depositos Registrados con exito", "success");
        this.loading = false;
        this.depositoService.notificarCambio.emit();

        let i = 0;
        let cantidad = arrayDeposito.length;
        for (i; i <= cantidad; i++) {
          this.agregarClientesADeposito.pop();
        }
      },
      err => {
        Swal.fire(
          'Error',
          'Hubo errores al realizar la transaccion de pagos',
          'error'
        );
      }
    );
  }

  public finalizarPagos():void{
    Swal.fire({
        title: 'Seguro que desea?',
        text: `Finalizar pagos para la agencia de  ${this.ubicacion} !`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, Finalizar !'
      }).then((result) => {

        if (result.value) {
          this.loading=true;
          this.fechaPagoService.finalizarPagos(this.ubicacion).subscribe(
            response=>{
              this.loading=false;
              Swal.fire('Exito','Operacion realizada con exito','success')
            },
            err=>{
              this.loading=false;
              Swal.fire('Error', 'Hubo un error en la operacion deseada, intente nuevamente','error');
            }
          )};
      })
  }




  /**************************GENERAR PDF************************** */
  generarpdf() {
    let doc = new jsPDF();
    doc.setFontSize(10);
    doc.text(`Ruta ${this.numeral} ${this.formaPago} de ${this.ubicacion}. Es responsable: ${this.personal.nombre} ${this.personal.apellido}`, 15, 15);
      doc.autoTable({
        head: this.headRowsVencidas(),
        body: this.bodyRowsVencidas(),
        startY:17,
        showHead: "firstPage",
        styles: { cellPadding: 0.5, fontSize: 8 },
        bodyStyles:{
          textColor:[231,76,60]
        },
        tableLineColor: [231, 76, 60],
        tableLineWidth: 0.5,
        willDrawCell: function(data) {
          if (data.row.section === "body"  && data.column.dataKey === "vencidas") {
            if(data.cell.raw>1)
            {
               doc.setTextColor(231, 76, 60); // Red
               doc.setFontStyle('bold');
            }
          }
        },
        columnStyles: {
          nombre: {
              fontStyle: 'bold'
          },
          apellido: {
            fontStyle: 'bold'
          },
          total:{
            fontStyle: 'bold'
          },
          mora:{
            fontStyle: 'bold'
          },
          totalc:{
            fontStyle: 'bold'
          }
      }
      });
    
    
    doc.autoTable({
      head: this.headRows(),
      body: this.bodyRows(),
      foot: this.footRows(),
      startY: doc.autoTable.previous.finalY + 5,
      showHead: "firstPage",
      styles: { cellPadding: 0.5, fontSize: 8 },
      willDrawCell: function(data) {
        if (data.row.section === "body"  && data.column.dataKey === "vencidas") {
          if(data.cell.raw>1)
          {
             doc.setTextColor(231, 76, 60); // Red
             doc.setFontStyle('bold');
          }
        }
      },
      columnStyles: {
        nombre: {
            fontStyle: 'bold'
        },
        apellido: {
          fontStyle: 'bold'
        },
        total:{
          fontStyle: 'bold'
        },
        mora:{
          fontStyle: 'bold'
        },
        totalc:{
          fontStyle: 'bold'
        }
    }
    });
    doc.save(`Reporte para ${this.personal.ruta.codigo}`);
  }

  headRows() {
    return [
      {
        no: "No.",
        codigo: "Código",
        nombre: "Nombre",
        apellido: "Apellido",
        telefono: "Teléfono",
        capital: "Capital",
        saldo: "Saldo",
        cuotas: "Cuotas",
        cp: "CP",
        vencidas: "Vencidas",
        cuota: "Cuota",
        total: "Total",
        mora: "Mora",
        totalc: "Total c"
      }
    ];
  }

  headRowsVencidas() {
    return [
      {
        no: "No.",
        codigo: "Código",
        nombre: "Nombre",
        apellido: "Apellido",
        telefono: "Teléfono",
        vencida:"Vencida el",
        capital: "Capital",
        saldo: "Saldo",
        mora: "Mora",
        totalc: "Total c"
      }
    ];
  }

  footRows() {
    return [
      {
        no: "",
        codigo: "Total:",
        nombre: "",
        apellido: "",
        telefono: "",
        capital: "",
        saldo: "",
        cuotas: "",
        cp: "",
        vencidas: "",
        cuota:this.sumarCuotas,
        total: this.sumCuotaMora,
        mora: this.sumarMora,
        totalc:this.sumTotal
      }
    ];
  }

  bodyRows() {
    let body = [];
    let contador: number = 0;
    let t = this.estadoCuentaActivo.length;
    for (var j = 1; j <= t; j++) {
      body.push({
        no: j,
        codigo: this.estadoCuentaActivo[contador].credito.cliente.codigo.codigo,
        nombre: this.estadoCuentaActivo[contador].credito.cliente.nombre,
        apellido: this.estadoCuentaActivo[contador].credito.cliente.apellido,
        telefono: this.estadoCuentaActivo[contador].credito.cliente.telefono,
        capital: this.estadoCuentaActivo[contador].credito.monto,
        saldo: this.estadoCuentaActivo[contador].saldo,
        cuotas: this.estadoCuentaActivo[contador].credito.tipoPago.noPago,
        cp: this.estadoCuentaActivo[contador].pagosRealizados,
        vencidas: this.estadoCuentaActivo[contador].credito.mora.diasMora + 1,
        cuota: this.estadoCuentaActivo[contador].credito.cuota,
        total:
        this.estadoCuentaActivo[contador].credito.cuota *
          (this.estadoCuentaActivo[contador].credito.mora.diasMora + 1),
        mora: this.estadoCuentaActivo[contador].credito.mora.moraGenerada,
        totalc:
        this.estadoCuentaActivo[contador].credito.cuota *
            (this.estadoCuentaActivo[contador].credito.mora.diasMora + 1) +
            this.estadoCuentaActivo[contador].credito.mora.moraGenerada
      });
      contador++;
    }
    return body;
  }

  bodyRowsVencidas() {
    let body = [];
    let contador: number = 0;
    let t = this.estadoCuentaCreditoVencido.length;
    for (var j = 1; j <= t; j++) {
      body.push({
        no: j,
        codigo: this.estadoCuentaCreditoVencido[contador].credito.cliente.codigo.codigo,
        nombre: this.estadoCuentaCreditoVencido[contador].credito.cliente.nombre,
        apellido: this.estadoCuentaCreditoVencido[contador].credito.cliente.apellido,
        telefono: this.estadoCuentaCreditoVencido[contador].credito.cliente.telefono,
        capital: this.estadoCuentaCreditoVencido[contador].credito.monto,
        saldo: this.estadoCuentaCreditoVencido[contador].saldo,
        vencida:this.estadoCuentaCreditoVencido[contador].credito.fechaVencimiento,
        mora: this.estadoCuentaCreditoVencido[contador].credito.mora.moraGenerada,
        totalc:
        this.estadoCuentaCreditoVencido[contador].saldo +
        this.estadoCuentaCreditoVencido[contador].credito.mora.moraGenerada
      });
      contador++;
    }
    return body;
  }

  /************************************************************************************************************* */

}
