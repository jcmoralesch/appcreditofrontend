import { Component, OnInit, ViewChild } from "@angular/core";
import { MatSort, MatPaginator, MatTableDataSource } from "@angular/material";
import { DepositoService } from "../../../model/service/deposito.service";
import { UsuarioService } from "../../../model/service/usuario.service";
import { LoginService } from "../../../model/service/login.service";
import { Deposito } from "../../../model/class/deposito";
import { RutaService } from "../../../model/service/ruta.service";
import { Ruta } from 'src/app/model/class/ruta';

@Component({
  selector: "app-historial-cobro",
  templateUrl: "./historial-cobro.component.html",
  styleUrls: ["./historial-cobro.component.css"]
})
export class HistorialCobroComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public ubicacion: string = "";
  public total: number = 0.0;
  public totalMora: number = 0.0;
  public dataSource: any;
  public rutas: Ruta[];

  constructor(
    private depositoService: DepositoService,
    private usuarioService: UsuarioService,
    private loginService: LoginService,
    private rutaService: RutaService
  ) {}

  ngOnInit() {
    this.findUsuarioById();
    this.cargarTableDesdePagos();
    this.findRutaByUbicacion();
  }

  displayedColumns = ["Nombre", "Apellido", "Cantidad", "Mora"];

  findUsuarioById(): void {
    this.usuarioService.findById(this.loginService.id).subscribe(ubicacion => {
      this.findDepositoByUbicacion(ubicacion.ubicacion);
      this.ubicacion = ubicacion.ubicacion;
    });
  }

  findDepositoByUbicacion(ubicacion: string): void {
    this.depositoService
      .findDepositoByUbicacion(ubicacion)
      .subscribe(response => {
        this.total = response
          .map(t => t.cantidad)
          .reduce((acc, value) => acc + value, 0);
        this.totalMora = response
          .map(t => t.mora)
          .reduce((acc, value) => acc + value, 0);
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();
      });
  }

  tableFilter(): (data: Deposito, filter: string) => boolean {
    let filterFunction = function(data, filter): boolean {
      return (
        data.credito.cliente.nombre.indexOf(filter) != -1 ||
        data.credito.cliente.apellido.indexOf(filter) != -1
      );
    };
    return filterFunction;
  }

  applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toUpperCase();
    this.dataSource.filter = filterValue;
  }

  findRutaByUbicacion(): void {
    this.usuarioService.findById(this.loginService.id).subscribe(ubicacion => {
      this.rutaService
        .findRutaByUbicacion(ubicacion.ubicacion)
        .subscribe(rutas => (this.rutas = rutas));
      this.ubicacion = ubicacion.ubicacion;
    });
  }

  findAllByFechaAndRuta(ruta: string) {
    this.depositoService.findDepositoByRuta(ruta).subscribe(
      response => {
        this.total = response
          .map(t => t.cantidad)
          .reduce((acc, value) => acc + value, 0);
        this.totalMora = response
          .map(t => t.mora)
          .reduce((acc, value) => acc + value, 0);
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();
      },
      err => {
        console.error("Observer tiene un error" + err);
      }
    );
  }

  private cargarTableDesdePagos(): void {
    this.depositoService.notificarCambio.subscribe(response =>
      this.findUsuarioById()
    );
  }
}
