import { Component, OnInit } from '@angular/core';
import {RutaService} from '../../model/service/ruta.service';
import {ActivatedRoute} from '@angular/router';
import {LoginService} from '../../model/service/login.service';
import { Ruta } from 'src/app/model/class/ruta';

@Component({
  selector: 'app-ruta',
  templateUrl: './ruta.component.html'
})
export class RutaComponent implements OnInit {
  title:'Rutas';
  ruta:Ruta[];
  paginador:any;

  constructor(
        private rutaService:RutaService,
        private activatedRoute:ActivatedRoute,
        public loginService:LoginService) { }

  ngOnInit() {
    this.findAll();
  }

  findAll():void{
    this.activatedRoute.paramMap.subscribe(
      params=>{
        let page:number=+params.get('page');
        if(!page){
          page=0;
        }
        this.rutaService.findAll(page).subscribe(
          response=>{
            this.ruta=response.content as Ruta[];
            this.paginador=response;
          });
      });
  }

}
