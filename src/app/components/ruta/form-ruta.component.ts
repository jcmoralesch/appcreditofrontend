import { Component, OnInit } from '@angular/core';
import {RutaService} from '../../model/service/ruta.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import { CodigoService } from '../../model/service/codigo.service';
import { Codigo } from '../../model/class/codigo';
import { Ruta } from 'src/app/model/class/ruta';

@Component({
  selector: 'app-form-ruta',
  templateUrl: './form-ruta.component.html'
})
export class FormRutaComponent implements OnInit {
  title:string='Formulario de registro';
  public ruta:Ruta = new Ruta();
  public errores:string[];
  private arrayCodigo:Array<Codigo>=[];
  public loading:boolean=false;

  constructor(private rutaService:RutaService,
              private router:Router,
              private codigoService:CodigoService) { }

  ngOnInit() {
  }

  store():void{
    this.loading=true;
    this.rutaService.store(this.ruta).subscribe(
      ruta=>{
        this.registrarCodigo(ruta);
        this.router.navigate(['/ruta']);
        this.loading=false;
        Swal.fire('Ruta registrada'    ,`Ruta registrada con exito`,'success');
      },
      err=>{
        this.loading=false;
        this.errores=err.error.errors as string[];
      }
    )
  }

  private registrarCodigo(ruta:Ruta):void{
    for(let i=1;i<=5;i++){
      let codigo:Codigo= new Codigo();
      codigo.codigo=ruta.codigo+i;
      codigo.ruta=ruta;
      this.arrayCodigo.push(codigo);
    }
    this.codigoService.storeAll(this.arrayCodigo).subscribe(
      response=>console.log('exito')
    );
  }


}
