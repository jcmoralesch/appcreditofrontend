import { Component, OnInit,ViewChild} from '@angular/core';
import {MatTableDataSource,MatSort,MatPaginator} from '@angular/material';
import {UsuarioService} from '../../model/service/usuario.service';
import {LoginService} from '../../model/service/login.service';
import {DepositoService} from '../../model/service/deposito.service';
import {Deposito} from '../../model/class/deposito';
import {RutaService} from '../../model/service/ruta.service';
import {DepositoConsulta} from './deposito-consulta';
import { Ruta } from 'src/app/model/class/ruta';

@Component({
  selector: 'app-deposito',
  templateUrl: './deposito.component.html',
  styleUrls: ['./deposito.component.css']
})
export class DepositoComponent implements OnInit {
  title:string='Depositos';
  ubicacion:string='';
  total:number=0.00;
  totalMora:number=0.00;
  dataSource: any;
  rutas:Ruta[];
  depositoConsulta:DepositoConsulta = new DepositoConsulta();
  validador:boolean=true;
  allRuta:Ruta[];
  rutasUbicacion:string[];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private usuarioService:UsuarioService,
              public loginService:LoginService,
              private depositoService:DepositoService,
              private rutaService:RutaService) { }

  ngOnInit() {
     this.findUsuarioById();
     this.findRutaByUbicacion();
     this.cargarRutasUbicacion();
  }

  displayedColumns = ['Ruta','Nombre','Apellido','Fecha','Hora','Cantidad','Mora'];

  findUsuarioById():void{
    this.usuarioService.findById(this.loginService.id).subscribe(
      ubicacion=>{
        this.findDepositoByUbicacion(ubicacion.ubicacion);
        this.ubicacion=ubicacion.ubicacion;
      }
    )
  }

  findDepositoByUbicacion(ubicacion:string):void{
    this.depositoService.findDepositoByUbicacion(ubicacion).subscribe(
      response=>{
        this.total=  response.map(t => t.cantidad).reduce((acc, value) => acc + value, 0);
        this.totalMora=  response.map(t => t.mora).reduce((acc, value) => acc + value, 0);
        this.dataSource=new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();

      })
  }

  tableFilter(): (data:Deposito, filter:string) => boolean {
      let filterFunction = function(data, filter): boolean {
       return data.credito.cliente.nombre.indexOf(filter) != -1
       || data.credito.cliente.apellido.indexOf(filter) != -1;

      }
      return filterFunction;
   }

   applyFilter(filterValue: any) {
     filterValue = filterValue.trim();
     filterValue = filterValue.toUpperCase();
     this.dataSource.filter=filterValue;
   }

   findRutaByUbicacion():void{
      this.usuarioService.findById(this.loginService.id).subscribe(
        ubicacion=>{
          this.rutaService.findRutaByUbicacion(ubicacion.ubicacion).subscribe(
            rutas=>this.rutas=rutas
          );
          this.ubicacion=ubicacion.ubicacion;
        }
      )
   }

   findAllByFechaAndRuta(ruta:string){
     this.depositoService.findDepositoByRuta(ruta).subscribe(
       response=>{
         this.total=  response.map(t => t.cantidad).reduce((acc, value) => acc + value, 0);
         this.totalMora=  response.map(t => t.mora).reduce((acc, value) => acc + value, 0);
         this.dataSource=new MatTableDataSource();
         this.dataSource.data=response;
         this.dataSource.sort=this.sort;
         this.dataSource.paginator=this.paginator;
         this.dataSource.filterPredicate=this.tableFilter();

       },
       err=>{
         console.error('Observer tiene un error' + err)
       }
     )
   }

   consultarDeposito():void{
     this.depositoService.findByDateBeetween(this.depositoConsulta.fecha1,this.depositoConsulta.fecha2,
                                            this.depositoConsulta.ubicacion,this.depositoConsulta.rta).subscribe(
       response=>{
         this.total=  response.map(t => t.cantidad).reduce((acc, value) => acc + value, 0);
         this.totalMora=  response.map(t => t.mora).reduce((acc, value) => acc + value, 0);
         this.dataSource=new MatTableDataSource();
         this.dataSource.data=response;
         this.dataSource.sort=this.sort;
         this.dataSource.paginator=this.paginator;
         this.dataSource.filterPredicate=this.tableFilter();
         this.validador=false;
       }
     )
   }

   cargarRutasUbicacion():void{
     this.rutaService.findAllRutaDistinct().subscribe(
       ubicacion=>this.rutasUbicacion=ubicacion
     )
   }

   findRutas(ubicacion:string){
     this.rutaService.findRutaByUbicacion(ubicacion).subscribe(
       rutas=>this.allRuta=rutas
     );
   }
}
