import { Component, OnInit,ViewChild} from '@angular/core';
import {EstadoCuentaService} from '../../model/service/estado-cuenta.service';
import { EstadoCuenta } from '../../model/class/estado-cuenta';
import {DepositoService} from '../../model/service/deposito.service';
import {MatTableDataSource,MatSort,MatPaginator} from '@angular/material';
import { Deposito } from '../../model/class/deposito';
import {InfoServiceService} from '../../model/service/info-service.service';
import {LoginService} from '../../model/service/login.service';
import {ToastrService} from 'ngx-toastr';
import {UsuarioService} from '../../model/service/usuario.service';
import {RutaService} from '../../model/service/ruta.service';
import Swal from 'sweetalert2';
import { MoraService } from '../../model/service/mora.service';
import { Mora } from '../../model/class/mora';
import { FechaPago } from '../../model/class/fecha-pago';
import { Credito } from '../../model/class/credito';

@Component({
  selector: 'app-estado-cuenta',
  templateUrl: './estado-cuenta.component.html',
  styleUrls:  ['./estado-cuenta.component.css']
})
export class EstadoCuentaComponent implements OnInit {
  estadoCuenta:EstadoCuenta;
  deposito:Deposito = new Deposito();
  title:string='Datos credito';
  estadoCuentaSeleccionado:EstadoCuenta;
  public fechaPagos:FechaPago[];
  dataSource: any;
  errores:string[];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ubicacion:string;
  rutasUbicacion:string[];
  mGenerada:number=0.00;
  public mostrarNavLateral:boolean=true;
  public texto:string =  'Ocultar';
  public loading:boolean=false;
  public  mora:Mora = new Mora();
  private cuotaCredito:number;

  constructor(public estadoCuentaService:EstadoCuentaService,
              private depositoService:DepositoService,
              private infoService:InfoServiceService,
              public loginService:LoginService,
              private toastr:ToastrService,
              private usuarioService:UsuarioService,
              private rutaService:RutaService,
              public moraService:MoraService) { }

  ngOnInit() {
    this.findUsuarioById();
    this.cargarRutasUbicacion();
  }

  displayedColumns = ['IC','Ruta',"Nombre",'Apellido','Cuotas','Cuota','Total','Saldo','TotalPagado','PagosFaltantes','Mora','Atraso','Adelantado','Acciones'];

  findUsuarioById():void{
     this.usuarioService.findById(this.loginService.id).subscribe(
         ubicacion=>{
           this.findAllEstadoCuenta(ubicacion.ubicacion);
           this.ubicacion=ubicacion.ubicacion;
         }
       )
  }

  findAllEstadoCuenta(ubicacion:string){
    this.estadoCuentaService.findAllEstadoCuenta(ubicacion).subscribe(
       response=>{
         this.dataSource=new MatTableDataSource();
         this.dataSource.data=response;
          //this.dataSource.filterPredicate=(data:EstadoCuenta,filter:any)=>data.credito.cliente.nombre.indexOf(filter) != -1;
          this.dataSource.sort=this.sort;
          this.dataSource.paginator=this.paginator;
         this.dataSource.filterPredicate=this.tableFilter();
       });
  }

  findAllByMoraUbicacion(ubicacion:string):void{
    this.estadoCuentaService.findAllByMoraUbicacion(ubicacion).subscribe(
      response=>{
        this.dataSource=new MatTableDataSource();
        this.dataSource.data=response;
         this.dataSource.sort=this.sort;
         this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
    )
  }

  findAllByAdelantadoUbicacion(ubicacion:string):void{
    this.estadoCuentaService.findAllAdelantadoUbicacion(ubicacion).subscribe(
      response=>{
        this.dataSource=new MatTableDataSource();
        this.dataSource.data=response;
         this.dataSource.sort=this.sort;
         this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
    )
  }

  findAllByFechaVencimiento(ubicacion:string):void{
    this.estadoCuentaService.findByFechaVencimiento(ubicacion).subscribe(
      response=>{
        this.dataSource=new MatTableDataSource();
        this.dataSource.data=response;
         this.dataSource.sort=this.sort;
         this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
    )
  }

  tableFilter(): (data: EstadoCuenta, filter: string) => boolean {
      let filterFunction = function(data, filter): boolean {
       return data.credito.cliente.nombre.indexOf(filter) != -1
       || data.credito.cliente.apellido.indexOf(filter) != -1
       || data.saldo.toString().indexOf(filter) != -1
       || data.credito.cliente.codigo.ruta.codigo.indexOf(filter) !=-1;
      }
      return filterFunction;

   }


  applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toUpperCase();
    this.dataSource.filter=filterValue;
  }

  registrarPago(row){
    this.estadoCuenta=row;
    this.mGenerada=row.credito.mora.moraGenerada;
    this.estadoCuentaService.abrirModal();
  }

  cerrarModal(){
    this.estadoCuentaService.cerrarModal();
  }

  infoCompleta(row:EstadoCuenta){
    this.estadoCuentaSeleccionado=row;
  
    this.estadoCuentaService.findAllFechaByCredito(row.credito).subscribe(
      response=>this.fechaPagos=response
    )
    this.infoService.abrirModal();
  }

  storeDeposito():void{
    let arrayDeposito:Array<Deposito>=[];
    this.loading=true;
    this.deposito.credito=this.estadoCuenta.credito;
    this.deposito.cantidad=this.estadoCuenta.credito.cuota;
    this.deposito.mora=this.estadoCuenta.credito.mora.moraGenerada;

    arrayDeposito.push(this.deposito);

    if(this.estadoCuenta.credito.cuota> this.estadoCuenta.saldo){
      Swal.fire('Error no se puede registrar','Esta ingresando una cantidad mayor al saldo','error');
      this.loading=false;
      return;
    }
    if(this.estadoCuenta.credito.mora.moraGenerada>this.mGenerada){
      Swal.fire('Error no se puede registrar','Esta ingresando una cantidad mayor a la mora','error');
      this.loading=false;
      return;
    }
    this.depositoService.storeArrayDeposito(arrayDeposito).subscribe(
      deposito=>{
         this.findUsuarioById();
         this.loading=false;
         this.cerrarModal();
         this.toastr.success(`Depósito de ${deposito.cantidad}  registrado`, "EXITO")
      },
      err=>{
        this.loading=false;
        Swal.fire('Error','No fue posible realizar el pago','error');
        this.errores=err.error.errors as string[];
      });
  }

  cargarRutasUbicacion():void{
    this.rutaService.findAllRutaDistinct().subscribe(
      ubicacion=>this.rutasUbicacion=ubicacion
    )
  }

  findAllByUbicacion(ubicacion:string){
    this.findAllEstadoCuenta(ubicacion);
  }

  public cambiaVisibilidad() {
    this.texto = (this.mostrarNavLateral) ?  'Mostrar' : 'Ocultar';
    this.mostrarNavLateral = !this.mostrarNavLateral; 
  }

  public cargarModalMora(row):void{
    this.cuotaCredito=row.credito.cuota;
    this.mora.diasAdelantado=row.credito.mora.diasAdelantado;
    this.mora.diasMora=row.credito.mora.diasMora;
    this.mora.moraGenerada=row.credito.mora.moraGenerada;
    this.mora.id=row.credito.mora.id;
    this.moraService.abrirModal();
  }

  public cerrarModalMora():void{
    this.moraService.cerrarModal();
  }

  public modificarMora():void{ 
    
    this.loading=true;
    this.mora.totalAdelantado=this.mora.diasAdelantado*this.cuotaCredito;
    this.mora.totalDeMora=this.mora.diasMora*this.cuotaCredito;
   
    this.moraService.updateMora(this.mora).subscribe(
      response=>{
        this.loading=false;
        this.findUsuarioById();
        this.cerrarModalMora();
        this.toastr.success('Mora actualizada con exito','Success'); 
      },
      err=>this.loading=false

    )

  }

}
