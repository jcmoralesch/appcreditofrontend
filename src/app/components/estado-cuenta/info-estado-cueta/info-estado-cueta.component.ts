import { Component, OnInit,Input } from '@angular/core';
import {InfoServiceService} from '../../../model/service/info-service.service';
import { EstadoCuenta } from '../../../model/class/estado-cuenta';
import { EstadoCuentaService } from '../../../model/service/estado-cuenta.service';
import { FechaPago } from '../../../model/class/fecha-pago';

@Component({
  selector: 'app-info-estado-cueta',
  templateUrl: './info-estado-cueta.component.html',
  styleUrls: ['./info-estado-cueta.component.css']
})
export class InfoEstadoCuetaComponent implements OnInit {
  title:string='Detalle completo de cuenta';
  //public fechasPago:FechaPago[];

  @Input() estadoCuenta:EstadoCuenta;
  @Input() fechasPago:FechaPago[];
  constructor(public infoService:InfoServiceService,
              public estadoCuentaService:EstadoCuentaService) { }

  ngOnInit() {

    /* this.estadoCuentaService.findAllFechaByCredito(this.estadoCuenta.credito).subscribe(
        response=>this.fechasPago=response
    ) */
  }

  cerrarModal(){
    this.infoService.cerrarModal();
  }
}
