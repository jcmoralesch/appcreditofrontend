import {Component,ViewChild} from '@angular/core';
import {MatTableDataSource,MatSort,MatPaginator} from '@angular/material';
import {CreditoService} from '../../model/service/credito.service';
import {Credito} from '../../model/class/credito';
import {LoginService} from '../../model/service/login.service';
import {UsuarioService} from '../../model/service/usuario.service';
import {TipoPago} from '../../model/class/tipo-pago';
import {ToastrService} from 'ngx-toastr';
import {RutaService} from '../../model/service/ruta.service';
import {CreditoConsulta} from './credito-consulta';
import Swal from 'sweetalert2';
import { Ruta } from 'src/app/model/class/ruta';
import { ClienteService } from 'src/app/model/service/cliente.service';

@Component({
  selector: 'app-credito',
  styleUrls: ['table-filtering-example.css'],
  templateUrl: './credito.component.html',
})
export class CreditoComponent {
  paginador:any;
  dataSource: any;
  ubicacion:string;
  credito:Credito=new Credito();
  tipoPagos:TipoPago[];
  total=0;
  rutas:Ruta[];
  allRuta:Ruta[];
  rutasUbicacion:string[];
  creditoConsulta:CreditoConsulta = new CreditoConsulta();
  validador:boolean=true;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public mostrarNavLateral:boolean=true;
  public texto: string =  'Ocultar';
  public loading:boolean=false;

  constructor(public creditoService:CreditoService,
              public loginService:LoginService,
              private usuarioService:UsuarioService,
              private clienteService:ClienteService,
              private toastr:ToastrService,
              private rutaService:RutaService){}

  ngOnInit() {
    this.findUsuarioById();
    this.findAllTipoPago();
    this.findRutaByUbicacion();
    this.cargarRutasUbicacion();
    this.cargarRutas();
  }

  displayedColumns = ['Ruta','Codigo',"Nombre",'Apellido','Cantidad','TP','Fecha','Acciones'];
  
  findUsuarioById():void{
    this.usuarioService.findById(this.loginService.id).subscribe(
      ubicacion=>{
        this.findCreditoByUbicacion(ubicacion.ubicacion);
        this.ubicacion=ubicacion.ubicacion;
      }
    )
  }

  findCreditoByUbicacion(ubicacion:string):void{
    this.creditoService.findCreditoByUbicacion(ubicacion).subscribe(
      response=>{
        this.total=  response.map(t => t.monto).reduce((acc, value) => acc + value, 0);
        this.dataSource=new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();

      })
  }

  tableFilter(): (data:Credito, filter:string) => boolean {
      let filterFunction = function(data, filter): boolean {
       return data.cliente.nombre.indexOf(filter) != -1
       || data.cliente.apellido.indexOf(filter) != -1
       || data.cliente.codigo.ruta.codigo.indexOf(filter) != -1;

      }
      return filterFunction;
   }

   applyFilter(filterValue: any) {
     filterValue = filterValue.trim();
     filterValue = filterValue.toUpperCase();
     this.dataSource.filter=filterValue;
   }

   abrirModal(row):void{
     this.credito=row;
     this.creditoService.abrirModal();
   }

   cerrarModal():void{
     this.creditoService.cerrarModal();
   }

   findAllTipoPago():void{
     this.clienteService.findAllTipoPago().subscribe(
       tipoPago=>{
         this.tipoPagos=tipoPago;
       }

     )
   }

   updateCredito():void{
     this.loading=true;
      this.creditoService.update(this.credito).subscribe(
        credito=>{
          this.loading=false;
          this.cerrarModal();
          this.findUsuarioById();
          this.toastr.success('Credito modificado correctamente');
        },
        err=>
        {
          this.loading=false;
          Swal.fire('ERROR','No fue posible realizar la midificacion','error');
        }
      )
   }

   findAllByFechaAndRuta(ruta:string){
     this.creditoService.findCreditoByRuta(ruta).subscribe(
       response=>{
         this.total=  response.map(t => t.monto).reduce((acc, value) => acc + value, 0);
         this.dataSource=new MatTableDataSource();
         this.dataSource.data=response;
         this.dataSource.sort=this.sort;
         this.dataSource.paginator=this.paginator;
         this.dataSource.filterPredicate=this.tableFilter();
       },
       err=>{
         console.error('Observer tiene un error' + err)
       }
     )
   }

   findRutaByUbicacion():void{
      this.usuarioService.findById(this.loginService.id).subscribe(
        ubicacion=>{
          this.rutaService.findRutaByUbicacion(ubicacion.ubicacion).subscribe(
            rutas=>this.rutas=rutas
          );
          this.ubicacion=ubicacion.ubicacion;
        }
      )
   }

   consultarCredito():void{
     this.creditoService.findByDateBeetween(this.creditoConsulta.fecha1,this.creditoConsulta.fecha2,
                                            this.creditoConsulta.ubicacion,this.creditoConsulta.rta).subscribe(
       response=>{
         this.total=  response.map(t => t.monto).reduce((acc, value) => acc + value, 0);
         this.dataSource=new MatTableDataSource();
         this.dataSource.data=response;
         this.dataSource.sort=this.sort;
         this.dataSource.paginator=this.paginator;
         this.dataSource.filterPredicate=this.tableFilter();
         this.validador=false;
       }
     )
   }

   cargarRutasUbicacion():void{
     this.rutaService.findAllRutaDistinct().subscribe(
       ubicacion=>this.rutasUbicacion=ubicacion
     )
   }

   cargarRutas():void{
     this.rutaService.findAllRuta().subscribe(
       rutas=>this.allRuta=rutas
     )
   }

   compararTipoPago(o1:TipoPago,o2:TipoPago):boolean{
     if(o1===undefined && o2===undefined){
       return true;
     }
     return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
   } 

   public cambiaVisibilidad() {
    this.texto = (this.mostrarNavLateral) ?  'Mostrar' : 'Ocultar';
    this.mostrarNavLateral = !this.mostrarNavLateral; 
  }

}
