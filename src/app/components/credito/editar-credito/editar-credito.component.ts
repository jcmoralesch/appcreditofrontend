import { Component, OnInit, ViewChild } from '@angular/core';
import { CreditoService } from '../../../model/service/credito.service';
import { MatTableDataSource, MatPaginator, MatSort, MatInput } from '@angular/material';
import { Credito } from '../../../model/class/credito';
import { Moment } from 'moment';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../../../model/service/login.service';
import Swal from 'sweetalert2';
import { RecordService } from '../../../model/service/record.service';
import { Record } from '../../../model/class/record';
import { RutaService } from '../../../model/service/ruta.service';
import { UsuarioService } from '../../../model/service/usuario.service';
import { Ruta } from 'src/app/model/class/ruta';

@Component({
  selector: 'app-editar-credito',
  templateUrl: './editar-credito.component.html',
  styleUrls: ['./editar-credito.component.css']
})
export class EditarCreditoComponent implements OnInit {

  public mostrarNavLateral:boolean=true;
  public dataSource:any;
  public ubicacion:string;
  public texto: string =  'Ocultar';
  public displayedColumns: string[] = ['ruta', 'codigo', 'nombre', 'apellido','cantidad','ciclo','fecharegistro','acciones'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatInput,{static:true}) matInput: MatInput;
  public credito:Credito = new Credito();
  public loading:boolean=false;
  date:Moment;
  public rutas:Ruta[];
  
  constructor(public creditoService:CreditoService,
              private toastrService:ToastrService,
              public loginService:LoginService,
              private recordService:RecordService,
              private rutaService:RutaService,
              private usuarioService:UsuarioService) { }

  ngOnInit() {
    this.findallCreditoStatusA();
    this.findRutaByUbicacion();
    
  }

  private findallCreditoStatusA():void{
    this.creditoService.findAllStatusA().subscribe(
      response=>{
        this.dataSource = new MatTableDataSource(response);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource.filterPredicate=this.tableFilter();
      }
    )
  }

  tableFilter(): (data:Credito, filter:string) => boolean {
    let filterFunction = function(data, filter): boolean {
     return data.cliente.nombre.indexOf(filter) != -1
     || data.cliente.apellido.indexOf(filter) != -1;
    }
    return filterFunction;
 }

  public cambiaVisibilidad() {
    this.texto = (this.mostrarNavLateral) ?  'Mostrar' : 'Ocultar';
    this.mostrarNavLateral = !this.mostrarNavLateral; 
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toUpperCase();
  }

  public abrirModa(element){
     this.credito=element;
     this.creditoService.abrirModal();
  }

  public updateCredito(){
     this.loading=true;
     if(this.date!=undefined){

       let fechaInicio= new Date(`${this.date.year()}/${this.date.month()+1}/${this.date.date()}`)
       this.credito.fechaRegistro=fechaInicio;
       this.credito.cliente.record.ciclo=this.credito.cliente.record.ciclo;
       this.creditoService.updateDateAndCiclo(this.credito).subscribe(
         response=>{
           this.loading=false;
           this.cerrarModal();
           this.date=null;
           this.toastrService.success('Datos crédito actualizados correctamente','success');
         },err=>{
           this.loading=false;
           Swal.fire('Error','Ocurrio un error en el servidor al realizar la operación','error');
         }
       )
     }else{
       let record:Record = new Record();
       record=this.credito.cliente.record;

       this.recordService.update(record).subscribe(
         response=>{
           this.loading=false;
           this.toastrService.success('Ciclo actualizado correctamente','Success');
           this.cerrarModal();

          }
       )
     }
  }

  public findAllStatusAAndRuta(ruta:string){
    this.creditoService.findAllStatusAAndRuta(ruta).subscribe(
      response=>{
        this.dataSource = new MatTableDataSource(response);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource.filterPredicate=this.tableFilter();
      },
      err=>{
        console.error('Observer tiene un error' + err)
      }
    )
  }

  public findRutaByUbicacion():void{
    this.usuarioService.findById(this.loginService.id).subscribe(
      ubicacion=>{
        this.rutaService.findRutaByUbicacion(ubicacion.ubicacion).subscribe(
          rutas=>this.rutas=rutas
        );
        this.ubicacion=ubicacion.ubicacion;
      }
    )
 }

  public cerrarModal(){
    this.creditoService.cerrarModal();
  }

}
