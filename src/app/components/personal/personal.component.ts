import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PersonalService} from '../../model/service/personal.service';
import {Personal} from '../../model/class/personal';
import Swal from 'sweetalert2';
import { UsuarioService } from '../../model/service/usuario.service';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html'
})
export class PersonalComponent implements OnInit {

  title:string="Personal";
  personal:Array<Personal>=[];
  paginador:any;

  constructor(private activatedRoute:ActivatedRoute,
              private personalService:PersonalService
              ) { }

  ngOnInit() {
    this.cargarPersonal();
  }

  public cargarPersonal():void{
    this.activatedRoute.paramMap.subscribe(
      params=>{
        let page:number= +params.get('page');
        if(!page){
          page=0;
        }
        this.personalService.getPersonal(page).subscribe(
          response=>{
            let addPersonal:Personal[];
            addPersonal=response.content as Personal[];
            this.paginador=response;

            addPersonal.forEach((prs:Personal)=>{
              this.personalService.verificarExisteUsuario(prs.id).subscribe(
              responseVal=>{
                prs.verificar=responseVal.existe;
                this.personal.push(prs);
              });
            });
          });
      });
  }

  delete(personal:any):void{
    Swal.fire({
        title: 'Esta seguro?',
        text: `De eliminar usuario al  ${personal.nombre} !`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, Eliminar !'
      }).then((result) => {

        if (result.value) {
         this.personalService.delete(personal.id).subscribe(
           response=>{
             this.personal=this.personal.filter(usr=> usr !==personal);
             Swal.fire(
               'Eliminado!',
               `El usuario ${personal.nombre} ha sido eliminado`,
               'success'
             )
           },
           err=>{
             this.cargarPersonal();
           }
         )

        }
      })
  }

}
