import { Component, OnInit } from '@angular/core';
import {Personal} from '../../model/class/personal';
import {PersonalService} from '../../model/service/personal.service';
import {Router,ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2';
import { Ruta } from 'src/app/model/class/ruta';


@Component({
  selector: 'app-form-personal',
  templateUrl: './form-personal.component.html'
})
export class FormPersonalComponent implements OnInit {

  title:string="Formulario registro personal";
  public personal:Personal = new Personal();
  public errores:string[];
  rutas:Ruta[];
  public loading:boolean=false;

  constructor(private personalService:PersonalService,
              private router:Router,
              private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.personalService.getRuta().subscribe(rutas=>this.rutas=rutas);
    this.cargarPersonal();
  }

  store():void{
      this.loading=true;
      this.personalService.store(this.personal).subscribe(
        personal=>{
          this.loading=false;
          this.router.navigate(['/personal']);
          Swal.fire('Personal nuevo',`${personal.nombre} ${personal.apellido} ha sido registrado con exito`,'success');
        },
        err=>{
          this.loading=false;
          this.errores=err.error.errors as string[];
        }
      )
  }

  cargarPersonal():void{
    this.activatedRoute.params.subscribe(
      params=>{
        let id=params['id'];
        if(id){
          this.personalService.getPersonalById(id).subscribe(
            personal=>this.personal=personal
          )
        }
      });
  }

  update():void{
    this.loading=true;
    this.personalService.udpate(this.personal).subscribe(
      personal=>{
        this.loading=false;
        this.router.navigate(['/personal']);
        Swal.fire('Datos Actualizados',`Se actualizaron datos para personal ${personal.nombre}`,'success');
      },
      err=>this.loading=false
    )
  }



  compararRuta(o1:Ruta,o2:Ruta):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

}
