import { Component, OnInit } from '@angular/core';
import {UsuarioService} from '../../../model/service/usuario.service';
import {Usuario} from '../../../model/class/usuario';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-usuario',
  templateUrl: './list-usuario.component.html',
  styleUrls: ['./list-usuario.component.css']
})
export class ListUsuarioComponent implements OnInit {

  usuarios:Usuario[];
  user:Usuario;
  public loading:boolean=false;

  constructor(public usuarioService:UsuarioService) { }

  ngOnInit() {
    this.findaAll();
  }

  findaAll():void{
    this.usuarioService.findAll().subscribe(
      usuarios=>{
        this.usuarios=usuarios;
      }
    )
  }

  mostrarDetalle(usuario:any){
    this.user=usuario;
    this.usuarioService.abirModal();
  }

  cerrarModal(){
    this.usuarioService.cerrarModal();
  }

  delete(usuario:any):void{

    Swal.fire({
        title: 'Esta seguro?',
        text: `De eliminar usuario al  ${usuario.username} !`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, Eliminar !'
      }).then((result) => {

        if (result.value) {
         this.usuarioService.delele(usuario).subscribe(
           response=>{
             this.usuarios=this.usuarios.filter(usr=> usr !==usuario);
             Swal.fire(
               'Eliminado!',
               `El usuario ${usuario.username} ha sido eliminado`,
               'success'
             )
           }
         )

        }
      })

  }

  public inactivar(usuario,val:number):void{
    this.loading=true;
    this.usuarioService.updateEnabled(usuario,val).subscribe(
      response=>{
        this.findaAll();
        if(response.enabled){
          Swal.fire('Activado',`Se han ACTIVADO los privilegios del Usuario ${usuario.username}`,'success');
        }
        else{
          Swal.fire('Inactivado',`El Usuario ${usuario.username} ha sido INACTIVADO`,'warning');
        }
         this.loading=false;
      },
      err=>this.loading=false
    )
  }

}
