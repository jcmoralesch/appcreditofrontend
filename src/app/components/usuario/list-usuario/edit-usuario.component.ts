import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {UsuarioService} from '../../../model/service/usuario.service';
import {Usuario} from '../../../model/class/usuario';
import {Role} from '../../../model/class/role';
import {RoleService} from '../../../model/service/role.service';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import {of} from 'rxjs';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-edit-usuario',
  templateUrl: './edit-usuario.component.html',
  styleUrls: ['./edit-usuario.component.css']
})
export class EditUsuarioComponent implements OnInit {
  usuario:Usuario = new Usuario();
  roleForm: FormGroup;
  roles:Role[]=[];

  constructor(private activatedRoute:ActivatedRoute,
              private usuarioService:UsuarioService,
              private fb:FormBuilder,
              private roleService:RoleService,
              private toastr:ToastrService,
              private router:Router) {

                          this.roleForm = this.fb.group({
                            username:[''],
                            password:[''],
                            roles: new FormArray([], minSelectedCheckboxes(1)),

                          });

                          of(this.roleService.findAll().subscribe(
                            roles=>{
                              this.roles=roles;
                              this.addCheckboxes();
                            }
                          ))

               }

  ngOnInit() {
    this.cargarUsuario();
  }

  private addCheckboxes() {
    this.roles.map((o, i) => {
      const control = new FormControl(i === 0); // if first item set to true, else false
      (this.roleForm.controls.roles as FormArray).push(control);
    });
  }

  submit() {
   const selectedOrderIds = this.roleForm.value.roles
     .map((v, i) => v ? this.roles[i] : null)
     .filter(v => v !== null);
   this.usuario.role=selectedOrderIds;

   this.usuarioService.update(this.usuario).subscribe(
       usuario=>{
         this.router.navigate(['/personal']);
         this.toastr.success("Usuario Modificado con exito", "Success");
       }
    );

 }



  cargarUsuario():void{

    this.activatedRoute.params.subscribe(
      params=>{
        let id=params['id'];
        if(id){
          this.usuarioService.findUsuarioById(id).subscribe(
            usuario=>{
              this.usuario=usuario;
            }
          )
        }
      });
  }

}

function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}
