import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn, Validators} from '@angular/forms';
import {RoleService} from '../../model/service/role.service';
import {Role} from '../../model/class/role';
import {of} from 'rxjs';
import {Usuario} from '../../model/class/usuario';
import {PersonalService} from '../../model/service/personal.service';
import {ActivatedRoute,Router} from '@angular/router';
import {Personal} from '../../model/class/personal';
import {UsuarioService} from '../../model/service/usuario.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  roleForm: FormGroup;
  roles:Role[]=[];
  usuario:Usuario = new Usuario();
  personal:Personal = new Personal();
  errores:string[];
  loading:boolean=false;

  constructor(private fb:FormBuilder,
              private roleService:RoleService,
              private personalService:PersonalService,
              private activatedRoute:ActivatedRoute,
              private usuarioService:UsuarioService,
              private toastr:ToastrService,
              private router:Router) {

  }

  cargarPersonal():void{
     this.activatedRoute.params.subscribe(params=>{
       let id=params['id']
       if(id){
         this.personalService.getPersonalById(id).subscribe(
           personal=>this.personal=personal
         )
       }
     });
  }

  ngOnInit() {
    this.cargarPersonal();
    this.roleForm = this.fb.group({
      username:['', [Validators.required,Validators.minLength(4),Validators.maxLength(30)]],
      password:['',[Validators.required,Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,15}')]],
      roles: new FormArray([], minSelectedCheckboxes(1)),

    });
    of(this.roleService.findAll().subscribe(
      roles=>{
        this.roles=roles;
        this.addCheckboxes();
      }
    ))
  }

  private addCheckboxes() {
    this.roles.map((o, i) => {
      const control = new FormControl(i === 0); // if first item set to true, else false
      (this.roleForm.controls.roles as FormArray).push(control);
    });
  }

  submit() {
    this.loading=true;
   const selectedOrderIds = this.roleForm.value.roles
     .map((v, i) => v ? this.roles[i] : null)
     .filter(v => v !== null);

   this.usuario.personal=this.personal;
   this.usuario.username=this.roleForm.value.username;
   this.usuario.password=this.roleForm.value.password;
   this.usuario.role=selectedOrderIds;

   this.usuarioService.store(this.usuario).subscribe(
       usuario=>{
         this.loading=false
         this.router.navigate(['/personal']);
         this.toastr.success("Usuario creado con exito", "Success");
       },
       err=>{
         this.loading=false;
         this.errores=err.error.errors as string[];
       }
    );

 }


}

function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : { required: true };
  };

  return validator;


}
