import { Component, OnInit } from '@angular/core';
import {UsuarioService} from '../../../model/service/usuario.service';
import {Usuario} from '../../../model/class/usuario';
import {ActivatedRoute,Router} from '@angular/router';
import Swal from 'sweetalert2';
import {LoginService} from '../../../model/service/login.service';

@Component({
  selector: 'app-modificar-usuario',
  templateUrl: './modificar-usuario.component.html',
  styleUrls: ['./modificar-usuario.component.css']
})
export class ModificarUsuarioComponent implements OnInit {

  usuario:Usuario = new Usuario();
  public loading:boolean=false;

  constructor(private usuarioService:UsuarioService,
              private activatedRoute:ActivatedRoute,
              private router:Router,
              public loginService:LoginService) { }

  ngOnInit() {
    this.findUsuario();
  }

  findUsuario():void{
    this.activatedRoute.params.subscribe(
      params=>{
        let username=params['username'];
        if(username){
          this.usuarioService.findByUsername(username).subscribe(
            usuario=>this.usuario=usuario
          )
        }
      }
    )
  }

  update(){
    this.loading=true;
    this.usuarioService.updatePassUser(this.usuario).subscribe(
      usuario=>{
        this.loading=false;
        this.loginService.logout();
        this.router.navigate(['/login']);
        Swal.fire('Datos actualizados','Los datos fueron actualizados exitosamente','success');
      },
      err=>this.loading=false
    )
  }




}
