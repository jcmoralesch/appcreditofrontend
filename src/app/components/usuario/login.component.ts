import { Component, OnInit } from '@angular/core';
import {Usuario} from '../../model/class/usuario';
import {ToastrService} from 'ngx-toastr';
import {LoginService} from '../../model/service/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  title:string='Iniciar session!';
  usuario:Usuario;
  public loading:boolean=false;

  constructor(private toastr:ToastrService,
              public loginService:LoginService,
              private router:Router) {
     this.usuario= new Usuario();
   }

  ngOnInit() {

    if(this.loginService.isAuthenticated()){
      this.toastr.info("Ya ha iniciado session", 'Information');
      this.router.navigate(['/cliente']);
    }
  }

  login():void{
    this.loading=true;
    if(this.usuario.username==null || this.usuario.password==null){
      this.errorsmsg();
      this.loading=false;
      return;
    }

    this.loginService.login(this.usuario).subscribe(
      response=>{
        //console.log(response);
      //  let objetoPayload=JSON.parse(atob(response.access_token.split(".")[1]));
        //console.log(objetoPayload);
        this.loginService.guardarUsuario(response.access_token);
        this.loginService.guardarToken(response.access_token);
        let usuario=this.loginService.usuario;
        this.loading=false;
        this.router.navigate(['/pagos']);
        this.toastr.success('Bienvenido nuevamente',`${usuario.username}`);

      },
      err=>{
         this.loading=false;
        if(err.status==400){
          this.toastr.error("Usuario o Password Incorrecto",'Error')
        }
      }
    )
  }

  errorsmsg(){
    this.toastr.error("Usuario o Password vacios",'Error')

  }


}
