import { Component, OnInit } from "@angular/core";
import { CodigoService } from "../../../model/service/codigo.service";
import { Codigo } from "../../../model/class/codigo";
import { RutaService } from "../../../model/service/ruta.service";
import { UsuarioService } from "../../../model/service/usuario.service";
import { LoginService } from "../../../model/service/login.service";
import { ToastrService } from "ngx-toastr";
import { NgForm } from "@angular/forms";
import { Ruta } from 'src/app/model/class/ruta';

@Component({
  selector: "app-form-codigo",
  templateUrl: "./form-codigo.component.html",
  styleUrls: ["./form-codigo.component.css"]
})
export class FormCodigoComponent implements OnInit {
  public rutas: Ruta[];
  public obtenerRuta: Codigo = new Codigo();
  public cantidadCodigo: number;
  public ubicacion: string;
  private arrayCodigo: Array<Codigo> = [];
  public loading: boolean = false;

  constructor(
    public codigoService: CodigoService,
    private rutaService: RutaService,
    private usuarioService: UsuarioService,
    private loginService: LoginService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.findUsuarioById();
  }

  public cerrarModal(): void {
    this.codigoService.cerrarModal();
  }

  public store(ngForm: NgForm): void {
    this.codigoService
      .findAllCodigoByCodigo(this.obtenerRuta.ruta.codigo)
      .subscribe(response => this.storeAll(response, ngForm));
  }

  private storeAll(obtenerCodigos: Array<Codigo> = [], ngForm: NgForm): void {
    this.loading = true;
    let arr: Array<number> = [];
    obtenerCodigos.forEach((cod: Codigo) => {
      let codigos = cod.codigo.substring(this.obtenerRuta.ruta.codigo.length);
      arr.push(parseInt(codigos));
    });
    let max: number = Math.max.apply(null, arr);

    for (let i = 1; i <= this.cantidadCodigo; i++) {
      let codigo: Codigo = new Codigo();
      codigo.codigo = this.obtenerRuta.ruta.codigo + (max + i);
      codigo.ruta = this.obtenerRuta.ruta;
      this.arrayCodigo.push(codigo);
    }
    this.codigoService.storeAll(this.arrayCodigo).subscribe(
      response => {
        this.codigoService.notificarCambio.emit();
        this.toastr.success("Codigos registrados con exito", "Success");
        this.cerrarModal();
        this.loading = false;
        let i = 0;
        let cantidad = this.arrayCodigo.length;
        for (i; i <= cantidad; i++) {
          this.arrayCodigo.pop();
        }
        ngForm.reset();
      },
      err => (this.loading = false)
    );
  }

  findUsuarioById(): void {
    this.usuarioService.findById(this.loginService.id).subscribe(response => {
      this.cargarRutas(response.ubicacion);
      this.ubicacion = response.ubicacion;
      this.cargarRutas(this.ubicacion);
    });
  }

  private cargarRutas(ubicacion: string): void {
    this.rutaService
      .findRutaByUbicacion(ubicacion)
      .subscribe(response => (this.rutas = response));
  }
}
