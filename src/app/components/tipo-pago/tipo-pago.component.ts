import { Component, OnInit } from '@angular/core';
import {TipoPagoService} from '../../model/service/tipo-pago.service';
import {TipoPago} from '../../model/class/tipo-pago';
import {LoginService} from '../../model/service/login.service';

@Component({
  selector: 'app-tipo-pago',
  templateUrl: './tipo-pago.component.html'
})
export class TipoPagoComponent implements OnInit {
  title:string="Tipo Pago";
  public tipoPago:TipoPago[];

  constructor(private tipoPagoService:TipoPagoService,
              public loginService:LoginService) { }

  ngOnInit() {
    this.findAll();
  }

  findAll():void{
    this.tipoPagoService.findAll().subscribe(
      response=>this.tipoPago=response
    )
  }



}
