import { Component, OnInit } from '@angular/core';
import {TipoPago} from '../../model/class/tipo-pago';
import {TipoPagoService} from '../../model/service/tipo-pago.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-tipo-pago',
  templateUrl: './form-tipo-pago.component.html'
})
export class FormTipoPagoComponent implements OnInit {

  title:string="Formulario Tipo Pago";
  public tipoPago:TipoPago = new TipoPago();
  public errores:string[];
  public loading:boolean=false;

  constructor(private tipoPagoService:TipoPagoService,
              private router:Router) { }

  ngOnInit() {
  }

  store():void{
    this.loading=true;
    this.tipoPagoService.store(this.tipoPago).subscribe(
      tipoPago=>{
        this.loading=false;
        this.router.navigate(['/tipo-pago']);
        Swal.fire('Nuevo tipo pago ',`${tipoPago.nombreTipo} registrado con exito`,'success');
      },
      err=>{
        this.loading=false;
        this.errores=err.error.errors as string[];
      }
    )
  }

  compararRuta(o1:TipoPago,o2:TipoPago):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

}
