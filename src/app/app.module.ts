import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, Component } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import {RouterModule,Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { TipoPagoComponent } from './components/tipo-pago/tipo-pago.component';
import { FormTipoPagoComponent } from './components/tipo-pago/form-tipo-pago.component';
import { PersonalComponent } from './components/personal/personal.component';
import { FormPersonalComponent } from './components/personal/form-personal.component';
import {HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatTableModule} from '@angular/material/table';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { RutaComponent } from './components/ruta/ruta.component';
import {FormRutaComponent} from './components/ruta/form-ruta.component';
import { PagRutaComponent } from './components/paginator/pag-ruta/pag-ruta.component';
import { CreditoComponent } from './components/credito/credito.component';
import { PagClienteComponent } from './components/paginator/pag-cliente/pag-cliente.component';
import { PagCreditoComponent } from './components/paginator/pag-credito/pag-credito.component';
import { EstadoCuentaComponent } from './components/estado-cuenta/estado-cuenta.component';
import {PagEstadoCuentaComponent} from './components/paginator/pag-estado-cuenta/pag-estado-cuenta.component';
import { DepositoComponent } from './components/deposito/deposito.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatFormFieldModule, MatInputModule,MatSortModule,MatPaginatorModule,MatButtonModule} from '@angular/material';
import { InfoEstadoCuetaComponent } from './components/estado-cuenta/info-estado-cueta/info-estado-cueta.component';
import { FechaPagoComponent } from './components/fecha-pago/fecha-pago.component';
import {MatSelectModule} from '@angular/material/select';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { LoginComponent } from './components/usuario/login.component';
import {ToastrModule} from 'ngx-toastr';
import {TokenInterceptor} from './components/usuario/interceptors/token.interceptor';
import {AuthInterceptor} from './components/usuario/interceptors/auth.interceptor';
import { ReactiveFormsModule } from '@angular/forms';
import { RoleComponent } from './components/role/role.component';
import { MoraComponent } from './components/mora/mora.component';
import { ListUsuarioComponent } from './components/usuario/list-usuario/list-usuario.component';
import { EditUsuarioComponent } from './components/usuario/list-usuario/edit-usuario.component';
import { ModificarUsuarioComponent } from './components/usuario/modificar-usuario/modificar-usuario.component';
import { registerLocaleData } from '@angular/common';
import localeESGTQ from '@angular/common/locales/es-GT';
import { EstadoCuentaFechaComponent } from './components/fecha-pago/estado-cuenta-fecha/estado-cuenta-fecha.component';
import { FormCodigoComponent } from './components/codigo/form-codigo/form-codigo.component';
import { HistorialCobroComponent } from './components/fecha-pago/historial-cobro/historial-cobro.component';
import { SoporteComponent } from './components/soporte/soporte.component';
import { EditarCreditoComponent } from './components/credito/editar-credito/editar-credito.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { FormClienteComponent } from './components/cliente/form-cliente.component';
import { FormActualizarComponent } from './components/cliente/form-actualizar/form-actualizar.component';
import { ClienteService } from './model/service/cliente.service';


registerLocaleData(localeESGTQ, 'es-GT');


const routes:Routes=[
  {path:'cliente',component:ClienteComponent},
  {path: 'cliente/form',component:FormClienteComponent},
  {path: 'cliente/page/:page',component:ClienteComponent},
  {path: 'personal',component:PersonalComponent},
  {path: 'personal/form',component:FormPersonalComponent},
  {path: 'personal/form/:id',component:FormPersonalComponent},
  {path: 'personal/page/:page',component:PersonalComponent},
  {path: 'tipo-pago',component:TipoPagoComponent},
  {path: 'tipo-pago/form',component:FormTipoPagoComponent},
  {path: 'ruta',component:RutaComponent},
  {path: 'ruta/form',component:FormRutaComponent},
  {path: 'ruta/page/:page',component:RutaComponent},
  {path: 'credito',component:CreditoComponent},
  {path: 'credito-editar',component:EditarCreditoComponent},
  {path: 'credito/page/:page',component:CreditoComponent},
  {path: 'estado-cuenta/page/:page',component:EstadoCuentaComponent},
  {path: 'estado-cuenta',component:EstadoCuentaComponent},
  {path: 'pagos',component:FechaPagoComponent},
  {path: 'login',component:LoginComponent},
  {path: 'usuario/form/:id',component:UsuarioComponent},
  {path:'cliente/form/:id',component:FormActualizarComponent},
  {path: 'usuario/list-usuario',component:ListUsuarioComponent},
  {path: 'usuario/list-usuario/edit/:id',component:EditUsuarioComponent},
  {path: 'usuario/update-user/:username',component:ModificarUsuarioComponent},
  {path: 'deposito/list-deposito', component:DepositoComponent},
  {path: 'soporte',component:SoporteComponent},
  {path:'**',redirectTo:'/login',pathMatch:'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    HeaderComponent,
    FooterComponent,
    FormClienteComponent,
    TipoPagoComponent,
    FormTipoPagoComponent,
    PersonalComponent,
    FormPersonalComponent,
    PaginatorComponent,
    RutaComponent,
    FormRutaComponent,
    PagRutaComponent,
    CreditoComponent,
    PagClienteComponent,
    PagCreditoComponent,
    EstadoCuentaComponent,
    PagEstadoCuentaComponent,
    DepositoComponent,
    InfoEstadoCuetaComponent,
    FechaPagoComponent,
    UsuarioComponent,
    LoginComponent,
    RoleComponent,
    FormActualizarComponent,
    MoraComponent,
    ListUsuarioComponent,
    EditUsuarioComponent,
    ModificarUsuarioComponent,
    EstadoCuentaFechaComponent,
    FormCodigoComponent,
    HistorialCobroComponent,
    SoporteComponent,
    EditarCreditoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatSelectModule,
    ToastrModule.forRoot(
      {closeButton:true}
    ),
    ReactiveFormsModule
  ],
  providers: [
    ClienteService,
    { provide: LOCALE_ID, useValue: 'es-GT' },
    {provide:HTTP_INTERCEPTORS,useClass: TokenInterceptor, multi:true},
    {provide:HTTP_INTERCEPTORS,useClass: AuthInterceptor, multi:true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
