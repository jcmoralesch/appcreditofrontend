import {TipoPago} from './tipo-pago';
import {EstadoCuenta} from './estado-cuenta';
import {Mora} from './mora'
import { Usuario } from './usuario';
import { Cliente } from './cliente';

export class Credito {
  monto:number=0.00;
  cliente:Cliente;
  tipoPago:TipoPago;
  status:string;
  cuota:number=0.00;
  estadoCuenta:EstadoCuenta;
  mora:Mora;
  id:number=0;
  public fechaRegistro;
  public fechaVencimiento;
  public montoTotal;
  usuario:Usuario;

}
