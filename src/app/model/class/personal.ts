import { Ruta } from './ruta';

export class Personal {
  id:number;
  nombre:string;
  ruta:Ruta;
  apellido:string;
  direccion:string;
  telefono:string;
  identificacion:string;
  public createAt;
  public verificar:string='';

}
