import {Personal} from './personal';
import {Role} from './role';

export class Usuario {
  id:number;
  username:string;
  password:string;
  role:Role[];
  personal:Personal;
  public enabled:boolean;
}
