import { Ruta } from './ruta';

export class Codigo {
    public codigo:string;
    public ruta:Ruta;
    public id:number;
}
