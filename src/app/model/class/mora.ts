import { Credito } from './credito';
export class Mora {
  public id:number;
  public diasMora:number;
  public moraGenerada:number;
  public diasAdelantado:number;
  public totalAdelantado:number;
  public totalDeMora:number;
  public credito:Credito;

}
