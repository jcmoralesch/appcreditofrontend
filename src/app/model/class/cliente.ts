import { TipoPago } from './tipo-pago';
import { Personal } from './personal';
import { Codigo } from './codigo';
import { Record } from './record';
export class Cliente {
  id:number;
  nombre:string='';
  apellido:string='';
  direccion:string='';
  telefono:string='';
  status:string='';
  foto:string='';
  cantidad:number;
  tipoPago:TipoPago;
  personal:Personal;
  codigo:Codigo;
  record:Record;
}
