export class TipoPago {
  id:number;
  nombreTipo:string;
  formaPago:string;
  interes:number;
  noPago:number;
  plazo:string;
}
