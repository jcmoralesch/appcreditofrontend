import {Credito} from './credito';
export class EstadoCuenta {
  id:number;
  saldo:any;
  credito:Credito;
  pagosFaltantes:number;
  totalPagado:any;
  estadoPago:string;
  pagosRealizados:number;
}
