import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Role} from '../class/role';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  urlEndPoint:string=environment.URL_BACKEND+'/api-credito/role';
  constructor(private http:HttpClient) { }

  findAll():Observable<Role[]>{
    return this.http.get<Role[]>(this.urlEndPoint);
  }
}
