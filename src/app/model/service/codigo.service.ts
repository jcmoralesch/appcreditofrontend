import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Codigo } from '../class/codigo';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CodigoService {

  private urlEndPoint:string=environment.URL_BACKEND+'/api-credito/codigo';
  public modal:boolean=false;
  private _notificarCambio= new EventEmitter<any>();

  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }


  constructor(private http:HttpClient) { }

  public findAllCodigoByCodigo(codigo:string):Observable<Codigo[]>{
    return this.http.get<Codigo[]>(`${this.urlEndPoint}/codigo/${codigo}`);
  }

  public findAllCodigoByRutaUbicacion(ubicacion:string):Observable<Codigo[]>{
    return this.http.get<Codigo[]>(`${this.urlEndPoint}/agencia/${ubicacion}`);
  }

  public findAllCodigoLibre(codigo:string):Observable<Codigo[]>{
    return this.http.get<Codigo[]>(`${this.urlEndPoint}/libre/${codigo}`);
  }

  public storeAll(codigos:Array<Codigo>):Observable<Codigo>{
    return this.http.post<Codigo>(`${this.urlEndPoint}`,codigos).pipe(
      map((response:any)=>response.codigo as Codigo),
      catchError(err=>{
        if(err.status==400){
          return throwError(err);
        }

        Swal.fire(err.error.mensaje,err.error.err,'error');
        return throwError(err);
      })
    )
  }

  public updateStatusCodigo(codigo:Codigo):Observable<Codigo>{
    return this.http.put<Codigo>(`${this.urlEndPoint}/${codigo.id}`,codigo);
  }
  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }
}
