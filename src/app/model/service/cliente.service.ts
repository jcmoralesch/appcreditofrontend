import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable,throwError} from 'rxjs';
import {map,catchError} from 'rxjs/operators';
import {Cliente} from '../class/cliente';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { TipoPago } from '../class/tipo-pago';
//import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  urlEndPoint:string=environment.URL_BACKEND+'/api-credito/cliente';
  modal:boolean=false;

  constructor(private http:HttpClient) { }

  store(cliente:Cliente):Observable<Cliente>{

    return this.http.post(this.urlEndPoint,cliente).pipe(
      map((response:any)=>response.cliente as Cliente),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public delete(id:number):Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(
        err=>{
          Swal.fire(err.error.mensaje,err.error.err,'error');
          return throwError(err);
        }
      )
    )
  }

  public findAllTipoPago():Observable<TipoPago[]>{
    return this.http.get<TipoPago[]>(this.urlEndPoint+'/tipo-pago');
  }

  findAllByUbicacion(ubicacion:string):Observable<Cliente[]>{
    return this.http.get<Cliente[]>(`${this.urlEndPoint}/${ubicacion}`);
  }

  public findAllByUbicacionAndStatus(ubicacion:string):Observable<Cliente[]>{
    return this.http.get<Cliente[]>(`${this.urlEndPoint}/status/${ubicacion}`);
  }

  public findAllByUbicacionAndStatusA(ubicacion:string):Observable<Cliente[]>{
    return this.http.get<Cliente[]>(`${this.urlEndPoint}/status/active/${ubicacion}`);
  }

  abrirModal():void{
    this.modal=true;
  }

  cerrarModal():void{
     this.modal=false;
  }

  updateDatosCliente(cliente:Cliente):Observable<Cliente>{
    return this.http.put<Cliente>(`${this.urlEndPoint}/${cliente.id}`,cliente);
  }

  getCliente(id:number):Observable<Cliente>{
    return this.http.get<Cliente>(`${this.urlEndPoint}/form/${id}`);
  }

  updateCliente(cliente:Cliente):Observable<Cliente>{
    return this.http.put<Cliente>(`${this.urlEndPoint}/update/${cliente.id}`,cliente);
  }

}
