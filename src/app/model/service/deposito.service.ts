import { Injectable, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Deposito } from "../class/deposito";
import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import Swal from "sweetalert2";
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: "root"
})
export class DepositoService {
  urlEndPoint: string = environment.URL_BACKEND + "/api-rest/deposito";
  private _notificarCambio = new EventEmitter<any>();

  constructor(private http: HttpClient) {}

  get notificarCambio(): EventEmitter<any> {
    return this._notificarCambio;
  }

  storeArrayDeposito(deposito: Array<Deposito>): Observable<Deposito> {
    return this.http.post(this.urlEndPoint, deposito).pipe(
      map((response: any) => response.deposito as Deposito),
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        console.error(e.error.mensaje);
        Swal.fire(e.error.mensaje, e.error.err, "error");

        return throwError(e);
      })
    );
  }

  findDepositoByUbicacion(ubicacion: string): Observable<Deposito[]> {
    return this.http.get<Deposito[]>(`${this.urlEndPoint}/${ubicacion}`);
  }

  findDepositoByRuta(ruta: string): Observable<Deposito[]> {
    return this.http.get<Deposito[]>(`${this.urlEndPoint}/ruta/${ruta}`);
  }

  findByDateBeetween(
    fecha1: string,
    fecha2: string,
    ubicacion: string,
    ruta: string
  ): Observable<Deposito[]> {
    return this.http.get<Deposito[]>(
      `${this.urlEndPoint}/consultar/${fecha1}/${fecha2}/${ubicacion}/${ruta}`
    );
  }
}
