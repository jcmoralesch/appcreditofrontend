import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {Observable,throwError} from 'rxjs';
import {catchError,map} from 'rxjs/operators';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment';
import { Ruta } from '../class/ruta';

@Injectable({
  providedIn: 'root'
})
export class RutaService {

  private urlEndPoint:string=environment.URL_BACKEND+'/api-ruta/ruta';
  private httHeaders= new HttpHeaders({'Content-Type': 'application/json'});


  constructor(private http:HttpClient) { }

  store(ruta:Ruta):Observable<Ruta>{

    return this.http.post<Ruta>(this.urlEndPoint,ruta,{headers:this.httHeaders}).pipe(
      map((response:any)=> response.ruta as Ruta),
      catchError(
        e=>{
          if(e.status==400){
            return throwError(e);
          }

          console.error(e.error.mensaje);
          Swal.fire(e.error.mensaje,e.error.err,'error');
          return throwError(e);
        }
      )
    )
  }

  findAll(page:number):Observable<any>{
    return this.http.get(this.urlEndPoint+'/page/'+page).pipe(
      map((response:any)=>{
        (response.content as Ruta[])
          return response;
      })
    )
  }

  findAllRuta():Observable<Ruta[]>{
    return this.http.get<Ruta[]>(this.urlEndPoint);
  }

  findAllRutaDistinct():Observable<string[]>{
    return this.http.get<string[]>(this.urlEndPoint+'/distinct');
  }

  findRutaByUbicacion(ubicacion:string):Observable<Ruta[]>{
    return this.http.get<Ruta[]>(`${this.urlEndPoint}/${ubicacion}`);
  }
}
