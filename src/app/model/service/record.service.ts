import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Record} from '../class/record';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecordService {

  private urlEndPoint:string=environment.URL_BACKEND+'/api-credito/record';

  constructor(private http:HttpClient) { }

  public update(record:Record):Observable<Record>{
    return this.http.put<Record>(`${this.urlEndPoint}/${record.id}`,record);
  }
}
