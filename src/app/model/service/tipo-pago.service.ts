import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http';
import {TipoPago} from '../class/tipo-pago';
import {map,catchError} from 'rxjs/operators';
import {Observable,throwError} from 'rxjs';
import Swal from 'sweetalert2';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipoPagoService {

  private urlEndPoint:string=environment.URL_BACKEND+'/api-credito-tipoPago/tipoPago';
  private httpHeaders= new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http:HttpClient) { }

  store(tipoPago:TipoPago):Observable<TipoPago>{
      return this.http.post(this.urlEndPoint,tipoPago,{headers:this.httpHeaders}).pipe(
         map((response: any)=>response.tipoPago as TipoPago),
         catchError(e=>{
             if(e.status==400){
               return throwError(e);
             }
             Swal.fire(e.error.mensaje,e.error.err,'error');
             return throwError(e);
         })
      )
  }

  findAll():Observable<TipoPago []>{

    return this.http.get<TipoPago[]>(this.urlEndPoint);
  }
}
