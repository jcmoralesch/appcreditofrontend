import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FechaPago} from '../class/fecha-pago';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FechaPagoService {

  urlEndPoint:string=environment.URL_BACKEND+'/api-credito/fecha-pago';
  modal:boolean=false;
  constructor(private http:HttpClient) { }

  findAllFechaPagoAndRuta(ruta:string):Observable<FechaPago[]>{
    return this.http.get<FechaPago[]>(this.urlEndPoint+'/'+ruta);
  }

  public finalizarPagos(ubicacion:string):Observable<any>{
    return this.http.get<any>(`${this.urlEndPoint}/finalizar/pago/${ubicacion}`);
  }

  abrirModal(){
    this.modal=true;
  }
  cerrarModal(){
    this.modal=false;
  }

}
