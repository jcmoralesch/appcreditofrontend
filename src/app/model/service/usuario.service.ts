import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Usuario} from '../class/usuario';
import {map,catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment';

import {LoginService} from './login.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private urlEndPoint:string=environment.URL_BACKEND+'/api-credito/usuario';
  modal=false;

  constructor(private http:HttpClient,private loginService:LoginService) { }

  store(usuario:Usuario):Observable<Usuario>{
    return this.http.post(this.urlEndPoint,usuario).pipe(
      map((response:any)=>response.usuario as Usuario),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.errors,'error');

        return throwError(e);
      })
    )
  }

  findById(id:number):Observable<any>{
    return this.http.get<any>(`${this.urlEndPoint}/ubicacion/${id}`).pipe(
      map((response:any)=>response)
    );
  }

  findUsuarioById(id:number):Observable<Usuario>{
    return this.http.get<Usuario>(`${this.urlEndPoint}/${id}`)
  }

  findAll():Observable<Usuario[]>{
    return this.http.get<Usuario[]>(this.urlEndPoint);
  }

  abirModal():void{
     this.modal=true;
  }

  cerrarModal():void{
    this.modal=false;
  }

  delele(usuario:Usuario):Observable<Usuario>{
    return this.http.put<Usuario>(`${this.urlEndPoint}/eliminar/${usuario.id}`,usuario).pipe(
      catchError(e=>{
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    );
  }

  public updateEnabled(usuario:Usuario,val:number):Observable<Usuario>{
    return this.http.put<Usuario>(`${this.urlEndPoint}/enabled/false-true/${usuario.id}/${val}`,usuario);
  }

  update(usuario:Usuario):Observable<Usuario>{
    return this.http.put<Usuario>(`${this.urlEndPoint}/${usuario.id}`,usuario);
  }

  updatePassUser(usuario:Usuario):Observable<Usuario>{
    return this.http.put<Usuario>(`${this.urlEndPoint}/update/${usuario.id}`,usuario);
  }

  findByUsername(username:string):Observable<Usuario>{
    return this.http.get<Usuario>(`${this.urlEndPoint}/actualizar/${username}`);
  }
}
