import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http';
import {Personal} from '../class/personal';
import {Observable,throwError} from 'rxjs';
import {map,catchError} from 'rxjs/operators';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment';
import { Ruta } from '../class/ruta';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {
  private urlEndPoint:string=environment.URL_BACKEND+'/api-credito-personal/personal';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});
  
  constructor(private http:HttpClient) { }

  store(personal:Personal):Observable<Personal>{
    return this.http.post(this.urlEndPoint,personal,{headers:this.httpHeaders}).pipe(
      map((response:any)=>response.personal as Personal),
      catchError(e=>{
          if(e.status==400){
            return throwError(e);
          }
          console.error(e.error.mensaje);
          Swal.fire(e.error.mensaje,e.error.err,'error');
          return throwError(e);
      })
    );
  }

  public verificarExisteUsuario(id:number):Observable<any>{
    return this.http.get<any>(`${this.urlEndPoint}/find-usuario/${id}`).pipe(
      map((response:any)=>response)
    );
    }

  getPersonal(page:number):Observable<any>{
    return this.http.get(this.urlEndPoint+'/page/'+page).pipe(
       map((response:any)=>{
           (response.content as Personal[]).map(
             personal=>{
               personal.nombre=personal.nombre.toUpperCase();
               return personal;
             });
             return response;
       })
    );
  }

  getRuta():Observable<Ruta[]>{
    return this.http.get<Ruta[]>(this.urlEndPoint+'/rutas');
  }

  getPersonalById(id:number):Observable<Personal>{
    return this.http.get<Personal>(`${this.urlEndPoint}/${id}`);
  }

  public getPersonalByRuta(ruta:string):Observable<Personal>{
    return this.http.get<Personal>(`${this.urlEndPoint}/find-by-ruta/${ruta}`);
  }

  udpate(personal:Personal):Observable<Personal>{
    return this.http.put<Personal>(`${this.urlEndPoint}/update/${personal.id}`,personal).pipe(
      map((response:any)=>response.personal as Personal),
      catchError(e=>{
          if(e.status==400){
            return throwError(e);
          }
          console.error(e.error.mensaje);
          Swal.fire(e.error.mensaje,e.error.err,'error');
          return throwError(e);
      })
    );
  }

  delete(id:number):Observable<Personal>{
    return this.http.delete<Personal>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(e=>{
        console.error(e.error.mensaje);
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    );
  }


}
