import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Credito} from '../class/credito';
import {Observable,throwError} from 'rxjs';
import {map,catchError} from 'rxjs/operators';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CreditoService {

  private urlEndPoint:string=environment.URL_BACKEND+'/api-credito/credito';
  modal:boolean=false;

  constructor(private http:HttpClient) { }

  store(credito:Credito):Observable<Credito>{
    return this.http.post(this.urlEndPoint,credito).pipe(
      map((response:any)=>response.credito as Credito),
      catchError(
        e=>{
          console.error(e.error.mensaje);
          Swal.fire(e.error.mensaje,e.error.errors,'error');
          return throwError(e);
        }
      )
    )
  }

  public findCreditoByUbicacion(ubicacion:any):Observable<Credito[]>{
    return this.http.get<Credito[]>(`${this.urlEndPoint}/${ubicacion}`);
  }

  public findAllStatusAAndRuta(ruta:any):Observable<Credito[]>{
    return this.http.get<Credito[]>(`${this.urlEndPoint}/find/status/active-ruta/${ruta}`);
  }

  public findCreditoByRuta(ruta:string):Observable<Credito[]>{
    return this.http.get<Credito[]>(`${this.urlEndPoint}/ruta/${ruta}`);
  }

  public update(credito:Credito):Observable<Credito>{
    return this.http.put<Credito>(`${this.urlEndPoint}/update/${credito.id}`,credito);
  }

  public findByDateBeetween(fecha1:string,fecha2:string,ubicacion:string,ruta:string):Observable<Credito[]>{
    return this.http.get<Credito[]>(`${this.urlEndPoint}/consultar/${fecha1}/${fecha2}/${ubicacion}/${ruta}`);
  }

  public findAllStatusA():Observable<Credito[]>{
    return this.http.get<Credito[]>(`${this.urlEndPoint}/find/status/active`);
  }

  public updateDateAndCiclo(credito:Credito):Observable<Credito>{
    return this.http.put<Credito>(`${this.urlEndPoint}/update/fecha-ciclo/${credito.id}`,credito);
  }

  abrirModal():void{
    this.modal=true;
  }

  cerrarModal():void{
    this.modal=false;
  }

}
