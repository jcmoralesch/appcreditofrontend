import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Mora } from '../class/mora';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MoraService {

  public modal:boolean=false;
  private urlEndPoint:string=environment.URL_BACKEND+'/api-credito/mora';

  constructor(private http:HttpClient) { }

  public updateMora(mora:Mora):Observable<Mora>{
    return this.http.put<Mora>(`${this.urlEndPoint}/${mora.id}`,mora).pipe(
      map((response:any)=>response.mora as Mora),
      catchError(
        err=>{
          if(err.status==400){
            return throwError(err);
          }
          Swal.fire(err.error.mensaje,err.error.err,'error');
          return throwError(err);
        }
      )
    )
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }
}
