import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EstadoCuenta} from '../class/estado-cuenta';
import { FechaPago } from '../class/fecha-pago';
import { Credito } from '../class/credito';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EstadoCuentaService {
  private urlEndPoint:string=environment.URL_BACKEND+'/api-credito/estado-cuenta';
  modal:boolean =false;

  constructor(private http:HttpClient) { }

    public findAllEstadoCuenta(ubicacion:string):Observable<EstadoCuenta[]>{
      return this.http.get<EstadoCuenta[]>(`${this.urlEndPoint}/${ubicacion}`);
    }

    public findAllByMoraUbicacion(ubicacion:string):Observable<EstadoCuenta[]>{
      return this.http.get<EstadoCuenta[]>(`${this.urlEndPoint}/mora/${ubicacion}`);
    }

    public findAllByUbicacionForReporte(ubicacion:string):Observable<EstadoCuenta[]>{
      return this.http.get<EstadoCuenta[]>(`${this.urlEndPoint}/reporte/ruta/${ubicacion}`)
    }

    public findAllFechaByCredito(credito:Credito):Observable<FechaPago[]>{
      return this.http.get<FechaPago[]>(`${this.urlEndPoint}/fechas/pago/${credito.id}`);
    }

    public findAllAdelantadoUbicacion(ubicacion:string):Observable<EstadoCuenta[]>{
      return this.http.get<EstadoCuenta[]>(`${this.urlEndPoint}/adelantado/${ubicacion}`);
    }

    public findByFechaVencimiento(ubicacion:string):Observable<EstadoCuenta[]>{
      return this.http.get<EstadoCuenta[]>(`${this.urlEndPoint}/fecha-ubicacion/${ubicacion}`)
    }

    abrirModal(){
      this.modal=true;
    }
    cerrarModal(){
      this.modal=false;
    }
}
